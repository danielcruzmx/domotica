#include <SPI.h>

 // define max7219 registers
#define MAX7219_REG_DECODEMODE   0x9
#define MAX7219_REG_INTENSITY    0xA
#define MAX7219_REG_SCANLIMIT    0xB
#define MAX7219_REG_SHUTDOWN     0xC
#define MAX7219_REG_DISPLAYTEST  0xF

#define CS_PIN 15             // D8 del ESP8266

#define SALA          1
#define RECAMARAS     2

void sendByte (const byte reg, const byte data){    
  digitalWrite (CS_PIN, LOW);
  SPI.transfer (reg);
  SPI.transfer (data);
  digitalWrite (CS_PIN, HIGH); 
}  // end of sendByte
 
void setup () {
  pinMode(CS_PIN, OUTPUT);
 
  SPI.begin ();
  SPI.setDataMode(SPI_MODE0);
  SPI.setClockDivider(SPI_CLOCK_DIV128);
   
  sendByte (MAX7219_REG_SCANLIMIT, 7);   // show 6 digits
  sendByte (MAX7219_REG_DECODEMODE, 0);  // using an led matrix (not digits)
  sendByte (MAX7219_REG_DISPLAYTEST, 0); // no display test
  sendByte (MAX7219_REG_INTENSITY, 10);  // character intensity: range: 0 to 15
  sendByte (MAX7219_REG_SHUTDOWN, 1);   // not in shutdown mode (ie. start it up)

  clear(SALA);
  clear(RECAMARAS);
  
} 

void clear(int linea){
  sendByte (linea, 0x00);
}
 
void loop ()  {
  test_on_off(SALA);
  delay(300);
  test_on_off(RECAMARAS);
} 

void test_on_off(int linea){
    byte v = 0x80;
    byte r = v;
    for(int i=1; i<=8; i++){
      clear(linea);
      delay(200);
      sendByte(linea,r);
      delay(200);
      r = v >> i; 
    }
    clear(linea);
}
