#include <FS.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h >
#include <WiFiManager.h>
#include <ArduinoJson.h>  
 
#define RELAY 0 // relay connected to  GPIO0

WiFiServer server(80);

int touchState = 0; 
int value = 0;
char output[2] = "5";    // touch initial GPIO5

bool shouldSaveConfig = false;
String outputState = "OFF";
String header;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config callback");
  shouldSaveConfig = true;
}
 
void setup() 
{
  Serial.begin(115200); 

  //SPIFFS.format();    // First Time

  Serial.println("mounting FS...");
  if (SPIFFS.begin()) {
    Serial.println("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);
        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json");
          strcpy(output, json["output"]);
        } else {
          Serial.println("failed to load json config");
        }
      } else {
        Serial.println("cannot open file");
      }
    } else {
       Serial.println("not exists file");
    }
  } else {
    Serial.println("failed to mount FS");
  }
  
  WiFiManagerParameter custom_output("output", "output", output, 2);
 
  WiFiManager wifiManager;
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.addParameter(&custom_output);

  if(!wifiManager.autoConnect("DCCH8266")){
    Serial.println("Fail to connected (timeout)");
    ESP.reset();
    delay(1000);
  }

  Serial.println("Connected");
  strcpy(output, custom_output.getValue());

   //save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["output"] = output;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
  }

  pinMode(RELAY,OUTPUT);
  pinMode(atoi(output),INPUT);
  digitalWrite(RELAY, LOW);

  // print GPIO's
  Serial.print("Puerto del relay: ");
  Serial.println(RELAY);
  Serial.print("Puerto del touch: ");
  Serial.println(output);
  
  // Start the server
  server.begin();
  Serial.println("Server started");
 
  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print(WiFi.localIP());
  Serial.println("/");
}
 
void loop() 
{

  // touch logic  
  touchState = digitalRead(atoi(output));
  if (touchState) {
    Serial.println("TOUCH");
    if(value == HIGH){
      value = LOW;
      digitalWrite(RELAY,LOW);
      Serial.println("RELAY=ON");
      outputState = "on";
    } else {
      value = HIGH;
      digitalWrite(RELAY,HIGH);
      Serial.println("RELAY=OFF");
      outputState = "off";
    }
    delay(1000);
  }

  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client)   {
    return;
  } else {
    Serial.println("New Client.");         
    String currentLine = "";               
    while (client.connected()) {           
      if (client.available()) {            
        char c = client.read();            
        Serial.write(c);                   
        header += c;
        if (c == '\n') {                   
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            if (header.indexOf("GET /relay/on") >= 0) {
              Serial.println("Relay on");
              outputState = "on";
              digitalWrite(RELAY, HIGH);
            } else if (header.indexOf("GET /relay/off") >= 0) {
              Serial.println("Relay off");
              outputState = "off";
              digitalWrite(RELAY, LOW);
            }
            
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #195B6A; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #77878A;}</style></head>");
            
            client.println("<body><h1>LUZ RECAMARA</h1>");
            
            client.println("<p>Relay Output - State " + outputState + "</p>");

            if (outputState=="off") {
              client.println("<p><a href=\"/relay/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/relay/off\"><button class=\"button button2\">OFF</button></a></p>");
            }                  
            client.println("</body></html>");
            
            client.println();
            break;
            
          } else { 
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }
}