#include <SPI.h>
#include <Wire.h>
#include "Adafruit_MPR121.h"

#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif

 // define max7219 registers
#define MAX7219_REG_DECODEMODE   0x9
#define MAX7219_REG_INTENSITY    0xA
#define MAX7219_REG_SCANLIMIT    0xB
#define MAX7219_REG_SHUTDOWN     0xC
#define MAX7219_REG_DISPLAYTEST  0xF

#define CS_PIN 15             // D8 del ESP8266

#define SALA          1
#define RECAMARAS     2

#define UNO_ON      0x80
#define DOS_ON      0x20
#define TRES_ON     0x08
#define CUATRO_ON   0x02
#define UNO_OFF     0x40
#define DOS_OFF     0x10
#define TRES_OFF    0x04
#define CUATRO_OFF  0x01

Adafruit_MPR121 cap = Adafruit_MPR121();
uint16_t lasttouched = 0;
uint16_t currtouched = 0;

byte reg[2] = { 0x00, 0x00 };
 
void setup () {

  pinMode(CS_PIN, OUTPUT);
 
  Serial.begin(9600);
  while (!Serial) { 
    delay(10);
  }

  Serial.println("Adafruit MPR121 Capacitive Touch sensor test"); 
  // Default address is 0x5A
  if (!cap.begin(0x5A)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  Serial.println("MPR121 found!");
  
  SPI.begin ();
  SPI.setDataMode(SPI_MODE0);
  SPI.setClockDivider(SPI_CLOCK_DIV128);
   
  sendByte (MAX7219_REG_SCANLIMIT, 7);   // show 6 digits
  sendByte (MAX7219_REG_DECODEMODE, 0);  // using an led matrix (not digits)
  sendByte (MAX7219_REG_DISPLAYTEST, 0); // no display test
  sendByte (MAX7219_REG_INTENSITY, 10);  // character intensity: range: 0 to 15
  sendByte (MAX7219_REG_SHUTDOWN, 1);   // not in shutdown mode (ie. start it up)

  clear(SALA);
  clear(RECAMARAS);
  todos_apagados();
  draw_tablero();
} 

void loop ()  {
  
  currtouched = cap.touched();
  
  for (uint8_t i=0; i<12; i++) {
    // it if *is* touched and *wasnt* touched before, alert!
    if ((currtouched & _BV(i)) && !(lasttouched & _BV(i)) ) {
      Serial.print(i); Serial.println(" touched");

      switch(i){
        case 1:
          Serial.println(" 1) casa/sala/a/luz/1/status");
          if( reg[0] & UNO_ON ) reg[0] = (reg[0] ^ UNO_ON) | UNO_OFF; 
          else reg[0] = (reg[0] ^ UNO_OFF) | UNO_ON; 
          break;
        case 2:
          Serial.println(" 2) casa/sala/a/luz/2/status");
          if( reg[0] & DOS_ON ) reg[0] = (reg[0] ^ DOS_ON) | DOS_OFF; 
          else reg[0] = (reg[0] ^ DOS_OFF) | DOS_ON; 
          break;
        case 3:
          Serial.println(" 3) casa/cocina/a/luz/1/status");
          if( reg[0] & TRES_ON ) reg[0] = (reg[0] ^ TRES_ON) | TRES_OFF;
          else reg[0] = (reg[0] ^ TRES_OFF) | TRES_ON;
          break;
        case 4:
          Serial.println(" 4) casa/servicio/a/luz/1/status");
          if( reg[0] & CUATRO_ON ) reg[0] = (reg[0] ^ CUATRO_ON) | CUATRO_OFF;
          else reg[0] = (reg[0] ^ CUATRO_OFF) | CUATRO_ON; 
          break;
        case 5:
          Serial.println(" 5) casa/recamara/a/luz/1/status");
          if( reg[1] & UNO_ON ) reg[1] = (reg[1] ^ UNO_ON) | UNO_OFF; 
          else reg[1] = (reg[1] ^ UNO_OFF) | UNO_ON;
          break;    
        case 6:
          Serial.println(" 6) casa/recamara/b/luz/1/status");
          if( reg[1] & DOS_ON ) reg[1] = (reg[1] ^ DOS_ON) | DOS_OFF; 
          else reg[1] = (reg[1] ^ DOS_OFF) | DOS_ON; 
          break;    
        case 7:
          Serial.println(" 7) casa/recamara/c/luz/1/status");
          if( reg[1] & TRES_ON ) reg[1] = (reg[1] ^ TRES_ON) | TRES_OFF;
          else reg[1] = (reg[1] ^ TRES_OFF) | TRES_ON;
          break;    
        case 8:
          Serial.println(" 8) casa/pasillo/a/luz/1/status");
          if( reg[1] & CUATRO_ON ) reg[1] = (reg[1] ^ CUATRO_ON) | CUATRO_OFF;
          else reg[1] = (reg[1] ^ CUATRO_OFF) | CUATRO_ON; 
          break;    
      }
    }
    // if it *was* touched and now *isnt*, alert!
    if (!(currtouched & _BV(i)) && (lasttouched & _BV(i)) ) {
      //Serial.print(i); Serial.println(" released");
    }
  }

  lasttouched = currtouched;
  draw_tablero();
  delay(200);
} 

void sendByte (const byte reg, const byte data){    
  digitalWrite (CS_PIN, LOW);
  SPI.transfer (reg);
  SPI.transfer (data);
  digitalWrite (CS_PIN, HIGH); 
}  // end of sendByte

void clear(int linea){
  sendByte (linea, 0x00);
}

void draw_tablero(){
  sendByte(SALA,reg[0]);
  sendByte(RECAMARAS,reg[1]);
}

void todos_apagados(){
  reg[0] =  UNO_OFF | DOS_OFF | TRES_OFF | CUATRO_OFF;
  reg[1] =  UNO_OFF | DOS_OFF | TRES_OFF | CUATRO_OFF;
}

void todos_encendidos(){
  reg[0] =  UNO_ON | DOS_ON | TRES_ON | CUATRO_ON;
  reg[1] =  UNO_ON | DOS_ON | TRES_ON | CUATRO_ON;
}

void test_on_off_led(int linea){
    byte v = 0x80;
    byte r = v;
    for(int i=1; i<=8; i++){
      clear(linea);
      delay(200);
      sendByte(linea,r);
      delay(200);
      r = v >> i; 
    }
    clear(linea);
}
