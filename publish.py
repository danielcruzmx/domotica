import paho.mqtt.client as paho 
import time
import sys

# parametros
if(len(sys.argv)==3):
    cliente = str(sys.argv[1])
    topic   = str(sys.argv[2])
else:
    cliente = None
    topic = None

# definicion de callbacks
def on_publish(client,userdata,result):
    print("Publicando datos : ")

# inicio
if(cliente and topic):
    # set
    broker = "192.168.0.10"
    port   = 1883 

    client = paho.Client(cliente) 
    client.on_publish = on_publish 
    client.connect(broker,port,keepalive=60) 

    contador = 0

    # publicando cada 5 segundos
    try:
        while(contador < 60):
            if(contador % 2):
                payload = "ON"
            else:
                payload = "OFF"
            ret = client.publish(topic,payload) 
            print(payload);
            print(ret)
            time.sleep(5)
            contador = contador + 1

    except KeyboardInterrupt:
        # desconecta con CTRL C
        client.disconnect()
else:
    print("Faltan argumentos, ejecute -> python publish.py cliente topic")        