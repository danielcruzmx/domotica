import paho.mqtt.client as paho
import sys

# parametros
if(len(sys.argv)==2):
    topic = str(sys.argv[1])
else:
    topic = None

# definicion de callbacks        
def on_message(client, userdata, message):
  print("Recibiendo dato :")  
  print(str(message.payload.decode("utf-8")) ) 
  print("")

# inicio
if(topic):
    # set
    broker = "192.168.0.10" 
    port   = 1883 

    client = paho.Client("user") 
    client.on_message=on_message
   
    print("Conectando al broker  -> ",broker)
    client.connect(broker)
    print("Suscribiendo al topic -> ", topic)    
    client.subscribe(topic)

    # verifica continuamente 
    try:
        while 1:
            client.loop_start() 
    
    except KeyboardInterrupt:
       # desconecta con CTRL C
        client.disconnect()
else:
    print("Faltan argumentos, ejecute -> python subscribe.py topic")        