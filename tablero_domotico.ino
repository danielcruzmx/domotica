#include <SPI.h>
#include <Wire.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include "Adafruit_MPR121.h"

#ifndef _BV
#define _BV(bit) (1 << (bit)) 
#endif

 // define max7219 registers
#define MAX7219_REG_DECODEMODE   0x9
#define MAX7219_REG_INTENSITY    0xA
#define MAX7219_REG_SCANLIMIT    0xB
#define MAX7219_REG_SHUTDOWN     0xC
#define MAX7219_REG_DISPLAYTEST  0xF

#define CS_PIN 15 // D8 del ESP8266

#define UNO_ON     0x40  // 0  1  0  0   0  0  0  0   
#define UNO_OFF    0x02  // 0  0  0  0   0  0  1  0  

#define DOS_ON     0x20  // 0  0  1  0   0  0  0  0  
#define DOS_OFF    0x01  // 0  0  0  0   0  0  0  1  

#define TRES_ON    0x10  // 0  0  0  1   0  0  0  0  
#define TRES_OFF   0x04  // 0  0  0  0   0  1  0  0  

#define CUATRO_ON  0x80  // 1  0  0  0   0  0  0  0  
#define CUATRO_OFF 0x08  // 0  0  0  0   1  0  0  0  

Adafruit_MPR121 cap = Adafruit_MPR121();
uint16_t lasttouched = 0;
uint16_t currtouched = 0;

// pueden ser 4 lineas de 8 leds
byte reg[4] = { 0x00, 0x00, 0x00, 0x00 };

const char* ssid = "RedmiA4";
const char* password = "valeria1";
const char* mqtt_server = "192.168.43.30";

WiFiClient espClient;
PubSubClient client(espClient);
char* msg="ON";

void setup () {

  pinMode(CS_PIN, OUTPUT);

  setup_wifi();
  client.setServer(mqtt_server,1883);
  client.setCallback(callback);
 
  Serial.begin(9600);
  while (!Serial) { 
    delay(10);
  }

  Serial.println("Adafruit MPR121 Capacitive Touch sensor test"); 
  // Default address is 0x5A
  if (!cap.begin(0x5A)) {
    Serial.println("MPR121 not found, check wiring?");
    while (1);
  }
  Serial.println("MPR121 found!");
  
  SPI.begin ();
  SPI.setDataMode(SPI_MODE0);
  SPI.setClockDivider(SPI_CLOCK_DIV128);
   
  sendByte (MAX7219_REG_SCANLIMIT, 7);   // display digits 0,1,2,3,4,5,6,7
  sendByte (MAX7219_REG_DECODEMODE, 0);  // no decode
  sendByte (MAX7219_REG_DISPLAYTEST, 0); // no display test
  sendByte (MAX7219_REG_INTENSITY, 10);  // character intensity: range: 0 to 15
  sendByte (MAX7219_REG_SHUTDOWN, 1);    // not in shutdown mode (ie. start it up)

  test_on_off_led();
  
  test_luces_encendidas();
  draw_tablero();
  delay(300);
  
  test_luces_apagadas();
  draw_tablero();
  delay(300);
   
  clear();  
} 

void loop ()  {

  if (!client.connected()) {
      reconnect();
  }
  client.loop();
  
  currtouched = cap.touched();
  
  for (uint8_t i=0; i<12; i++) {
    // it if *is* touched and *wasnt* touched before, alert!
    if ((currtouched & _BV(i)) && !(lasttouched & _BV(i)) ) {
      Serial.print(i); Serial.println(" touched");

      switch(i){
        case 0:
          Serial.println(" 1) casa/luz/1/status");
          if( reg[0] & UNO_ON ){ // si bit es 1
            reg[0] = (reg[0] ^ UNO_ON) | UNO_OFF;  // apaga verde con XOR, enciende rojo  con OR
            msg = "OFF";
          } else {
            reg[0] = (reg[0] ^ UNO_OFF) | UNO_ON;  // apaga rojo  con XOR, enciende verde con OR
            msg = "ON";
          }
          client.publish("casa/luz/1/status",msg);  
          break;
        case 2:
          Serial.println(" 2) casa/luz/2/status");
          if( reg[0] & DOS_ON ) {
            reg[0] = (reg[0] ^ DOS_ON) | DOS_OFF; 
            msg = "OFF";
          } else {
            reg[0] = (reg[0] ^ DOS_OFF) | DOS_ON; 
            msg = "ON";
          }
          client.publish("casa/luz/2/status",msg);  
          break;
        case 7:
          Serial.println(" 3) casa/luz/3/status");
          if( reg[0] & TRES_ON ) {
            reg[0] = (reg[0] ^ TRES_ON) | TRES_OFF;
            msg = "OFF";
          } else {
            reg[0] = (reg[0] ^ TRES_OFF) | TRES_ON;
            msg = "ON";
          }
          client.publish("casa/luz/3/status",msg);  
          break;
        case 10:
          Serial.println(" 4) casa/luz/4/status");
          if( reg[0] & CUATRO_ON ) {
            reg[0] = (reg[0] ^ CUATRO_ON) | CUATRO_OFF;
            msg = "OFF";
          } else {
            reg[0] = (reg[0] ^ CUATRO_OFF) | CUATRO_ON; 
            msg = "ON";
          }
          client.publish("casa/luz/4/status",msg);  
          break;
      }
    }
    // if it *was* touched and now *isnt*, alert!
    if (!(currtouched & _BV(i)) && (lasttouched & _BV(i)) ) {
      //Serial.print(i); Serial.println(" released");
    }
  }

  lasttouched = currtouched;
  draw_tablero();
  delay(200);
} 

void sendByte (const byte reg, const byte data){    
  digitalWrite (CS_PIN, LOW);
  SPI.transfer (reg);
  SPI.transfer (data);
  digitalWrite (CS_PIN, HIGH); 
}  // end of sendByte

void clear(){
  sendByte (1, 0x00);
}

void draw_tablero(){
  sendByte(1, reg[0]);
}

void test_luces_apagadas(){
  reg[0] =  UNO_OFF | DOS_OFF | TRES_OFF | CUATRO_OFF; // leds rojos encendidos 0101 0101
}

void test_luces_encendidas(){
  reg[0] =  UNO_ON | DOS_ON | TRES_ON | CUATRO_ON;    // leds verdes encendidos 1010 1010
}

void test_on_off_led(){
    byte v = 0x80;
    byte r = v;
    // recorre un bit desde la posicion mas alta 0x80 hasta la posicion mas baja 0x00
    for(int i=1; i<=8; i++){
      clear();       // apaga todos
      delay(200);    // espera 
      sendByte(1,r); // enciende segun bit
      delay(200);    // espera 
      r = v >> i;    // recorre bit
    }
    clear();
}

void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
 
  if ((char)payload[0] == '1') {
    //digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
  } else {
    //digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      //client.publish(outTopic, message);
      // ... and resubscribe
      client.subscribe("casa/luz/+/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
